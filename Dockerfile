FROM daocloud.io/python:2-onbuild

MAINTAINER Vanderick<VanderLancer@gmail.com>

COPY hbase.tar /usr/src/app

RUN cp /usr/src/app/hbase.tar /usr/local/lib/python2.7/site-packages/ \
    && cd /usr/local/lib/python2.7/site-packages \
    && tar -xvf hbase.tar \
    && rm -f hbase.tar \
    && rm -f /usr/src/app/hbase.tar \
    && ln -sf /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime

